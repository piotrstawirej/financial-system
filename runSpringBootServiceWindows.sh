#!/usr/bin/env bash

JDK_PATH="c:\\Program Files\\Java\\jdk-12\\"
JAVA_HOME=${JDK_PATH} mvn spring-boot:run
